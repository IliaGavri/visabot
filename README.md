# README #

This is VisaBot  app 

### MVP v1. ###

* Админ. панель
    * Авторизация
    * Справочники
        * Справочник стран
        * Справочник визовых центров
    * Журнал операций
    * Метрики

* Бот
    * Меню:
        * Выгодные предложения
        * Процесс подачи документов в визовый центр (сохранение в Журнал операций, отброс метрик, сохранение черновика в MongoDB через Node.js)
        ** Процесс согласия клиента с обработкой ПДН (сохранение в Журнал операций, отброс метрик, сохранение черновика в MongoDB через Node.js)
        * Статус заявления

### figma ###

[VisaBot](https://www.figma.com/file/cMCExLuPCGltNeOJo2dxvZ/VisaBot?node-id=0%3A1)

### trello ###

[VisaBot](https://trello.com/b/OvDMwYe9/visabot)

### miro ###

[VisaBot](https://miro.com/app/board/uXjVOL5bQgY=/)
