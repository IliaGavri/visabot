const TelegramBot = require('node-telegram-bot-api');
const {run} = require('./mongo-client')

run()

// replace the value below with the Telegram token you receive from @BotFather
const token = '5154198187:AAH7arpEM6U0MmzeSScAIuMW0mbCgcndvW8';

// Create a bot that uses 'polling' to fetch new updates
const bot = new TelegramBot(token, {polling: true});

// Главное меню
const keyboardMain = [
  [
    {
      text: 'Выгодные предложения', // текст на кнопке
      callback_data: 'offers' // данные для обработчика событий
    }
  ],
  [
    {
      text: 'Процесс подачи документов',
      callback_data: 'sendDoc'
    }
  ],
  [
    {
      text: 'Статус заявления',
      callback_data: 'status'
    }
  ]
];

// Меню Выгодные предложения
const keyboard_01 = [
  [
    {
      text: 'Туры',
      callback_data: 'tours'
    }
  ],
  [
    {
      text: 'Отели',
      callback_data: 'hotels'
    }
  ]
];

// Меню Процесс подачи документов
const keyboard_02 = [
  [
    {
      text: 'Согласие',
      callback_data: 'confirm'
    }
  ]
];

// Matches "/echo [whatever]"
bot.onText(/\/echo (.+)/, (msg, match) => {
  // 'msg' is the received Message from Telegram
  // 'match' is the result of executing the regexp above on the text content
  // of the message

  const chatId = msg.chat.id;
  const resp = match[1]; // the captured "whatever"

  // send back the matched "whatever" to the chat
  bot.sendMessage(chatId, resp);
});

// Listen for any kind of message. There are different kinds of
// messages.
bot.on('message', (msg) => {
  const chatId = msg.chat.id;
  console.log(msg);
  // отправляем сообщение
  bot.sendMessage(chatId, 'Привет, ' + msg.from.first_name +'! чего хочешь?', { // прикрутим клаву
    reply_markup: {
        inline_keyboard: keyboardMain
    }
});
});

// обработчик событий нажатий на клавиатуру
bot.on('callback_query', (query) => {
  const chatId = query.message.chat.id;
  let messageText;
  let keyboard = null;

  if (query.data === 'offers') { // если Выгодные предложения
    messageText = 'Выгодные предложения';
    keyboard = keyboard_01;
  }

  if (query.data === 'sendDoc') { // если Процесс подачи документов 
    messageText = 'Подача документов';
    keyboard = keyboard_02;
  }

  if (query.data === 'status') { // если Просмотр статуса
    messageText = 'Просмотр статуса';
  }

  if (messageText) {
    if (keyboard) {
      bot.sendMessage(chatId, messageText, { // прикрутим клаву
          reply_markup: {
              inline_keyboard: keyboard
          }
      });
    } else {
      bot.sendMessage(chatId, messageText);
    }
  } else {
      bot.sendMessage(chatId, 'Непонятно, давай попробуем ещё раз?', { // прикрутим клаву
          reply_markup: {
              inline_keyboard: keyboardMain
          }
      });
  }
});