const e = require('express')
const {MongoClient} = require('mongodb')

const mongoClient = new MongoClient('mongodb://localhost:27017')

async function run() {
    try {
        await mongoClient.connect()

        const db = mongoClient.db('visabot')
        const collection = db.collection('data')

        const country = {name: 'Russia'}
        await collection.insertOne(country)

        const data = await collection.find().toArray()
        console.log(data)

    } catch (error) {
        console.log(error)
    } finally {
        mongoClient.close
    }
}

module.exports = {run}